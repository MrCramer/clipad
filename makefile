
CFLAGS = -Wall -c -I${HOME}/local/include -I${HOME}/local/include/ncurses -g
LFLAGS = -L${HOME}/local/lib -L${HOME}/local/include/ncurses -L${HOME}/local/include -lncurses


all: clipad

clipad: main.o
	gcc main.o ${LFLAGS} -o clipad

main.o: main.c
	gcc ${CFLAGS} main.c


clean:
	rm *.o
