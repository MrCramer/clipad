
#include <ncurses.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum mode_e {
  MODE_QUIT,
  MODE_MENU,
  MODE_EDIT
};

enum {
  COLOR_PAIR_MAIN,
  COLOR_PAIR_MENU,
  COLOR_PAIR_MENU_SELECTED,
  COLOR_PAIR_MENU_OPEN,
  COLOR_PAIR_MENU_OPEN_SEL
};


bool window_size_changed;


/* Document model stuff */
struct line_s {
  char *str;
  int length;
  int alloc;
};

struct {
  int nlines_used;     /* number of lines actually used (remainder are garbage) */
  int nlines_alloc;    /* length of lines and lines_lengths */
  struct line_s *lines;
} document = { .nlines_alloc = 0 };

void readInDocument(char *filename);
void resizeDocument(int num_lines);
void destroyDocument();

void docAddNewLine(int line_num, int col_num);
void docRmvNewLine(int line_num);
void docAddChar(char c, int line_num, int col_num);
void docRmvChar(int line_num, int col_num);


/* Edit view details */
enum mode_e modeEdit();

void initView();
void refreshView();
void renderView();


/* Menu Stuff */
enum mode_e modeMenu();

void initMenu();
void refreshMenu();
void destroyMenu();
void renderMenuBar();
void renderMenuOpen();


/* Undo stuff */

#define MAX_UNDOABLE_ACTIONS 100

struct action_s {
  enum  {
    NON_ACTN,
    ADD_LINE,
    RMV_LINE,
    ADD_CHAR,
    RMV_CHAR
  } type;

  int line_num, col_num;
  char c;
  struct action_s *next;
  struct action_s *prev;
};


struct action_list_s {
  struct action_s *front;
  struct action_s *back;
  int length;
} undo_list = { .front=NULL, .back=NULL, .length=0 },
  redo_list = { .front=NULL, .back=NULL, .length=0 };

void initUndoSystem();
void destroyUndoSystem();

void pushAction(struct action_list_s *list, struct action_s action);
struct action_s popAction (struct action_list_s *list);

void takeAction(struct action_list_s *action_list,
                struct action_list_s *undo_action_list);
void destroyActionList(struct action_list_s *list);



/* Called to kill the prog */
static void finish(int sig);


/* main, you know what main does right? */
int main(int argc, char **argv) {
  if(argc<2) {
    printf("Usage: clipad [file]\n");
    return 0;
  }

  signal(SIGINT, finish);

  readInDocument(argv[1]);
  
  /* Initialise ncurses */
  initscr();
  keypad(stdscr, TRUE);
  cbreak();
  noecho();
  start_color();
  wrefresh(stdscr);
  
  /* Initialise colours */
  init_pair(COLOR_PAIR_MENU,          COLOR_BLACK, COLOR_CYAN);
  init_pair(COLOR_PAIR_MENU_SELECTED, COLOR_BLACK, COLOR_WHITE);
  init_pair(COLOR_PAIR_MENU_OPEN,     COLOR_BLACK, COLOR_WHITE);
  init_pair(COLOR_PAIR_MENU_OPEN_SEL, COLOR_CYAN,  COLOR_WHITE);


  /* Initiliase systems */
  initMenu();
  initView();
  initUndoSystem();
  

  /* Run program */
  renderView();
  
  enum mode_e mode = MODE_EDIT;
  while(mode!=MODE_QUIT) {
    switch(mode) {
      case MODE_QUIT:
        break;
      case MODE_MENU:
        mode = modeMenu();
        break;
      case MODE_EDIT:
        mode = modeEdit();
        break;
    }
  }

   
  finish(0);
  return 0;
}


/* Called to kill the program */
static void finish(int sig) {
  endwin();

  destroyDocument();
  destroyMenu();

  exit(0);
}



/****************** 
 * Document Model * 
 ******************/

#define MIN_LINE_LENGTH 10;


/* Reads in a document into the document struct, kills the prog if cannot open file */
void readInDocument(char* filename) {
  FILE *fp = fopen(filename, "r");
  ssize_t bytes_read;
  size_t nbytes;
  char *buffer;

  if(fp==NULL) {
    fprintf(stderr, "readInDocument failed to open file %s\n", filename);
    finish(0);
  }

  document.nlines_alloc = 10;
  document.lines        = malloc(sizeof(struct line_s)*document.nlines_alloc);

  /* We assume here that there are no null (\0) characters in the file. */
  document.nlines_used = 0;

  nbytes = MIN_LINE_LENGTH;
  buffer = (char*) malloc(nbytes);
  bytes_read = getline(&buffer, &nbytes, fp);
  while(bytes_read != -1) {
    if(document.nlines_used == document.nlines_alloc) {
      resizeDocument(document.nlines_alloc*2);
    }
    buffer[bytes_read-1] = '\0'; /* strip off the newline char */

    document.lines[document.nlines_used].length = strlen(buffer);
    document.lines[document.nlines_used].alloc  = nbytes;
    document.lines[document.nlines_used].str    = buffer;
    document.nlines_used += 1;

    nbytes = MIN_LINE_LENGTH;
    buffer = (char*) malloc(sizeof(char)*nbytes);
    bytes_read = getline(&buffer, &nbytes, fp);
  }
  free(buffer);
}


/* Resizes the document to num_lines, kills prog if cannot realloc */
void resizeDocument(int num_lines) {
  struct line_s *tmp;

  // Don't bother making the doc smaller
  if(num_lines < document.nlines_alloc) return;

  tmp = realloc(document.lines, sizeof(struct line_s)*num_lines);
  if(tmp==NULL) {
    fprintf(stderr, "resizeDocument failed to realloc");
    finish(0);
  }
  document.lines = tmp;

  document.nlines_alloc = num_lines;
}


/* Cleans up memory alloc'ed for the document */
void destroyDocument() {
  int i;
  for(i=0; i<document.nlines_used; ++i) {
    free(document.lines[i].str);
  }
  free(document.lines);

  document.nlines_alloc = 0;
}


void docAddNewLine(int line_num, int col_num) {
  int i;
  int new_line_size = document.lines[line_num].length - col_num;

  /* create the new line */
  if(document.nlines_used >= document.nlines_alloc) {
    resizeDocument(document.nlines_used*2);
  }
  document.nlines_used += 1;

  for(i=document.nlines_used-1; i>line_num+1; i--) {
    document.lines[i] = document.lines[i-1];
  }
  document.lines[line_num+1].str    = (char*) malloc(new_line_size+1);
  document.lines[line_num+1].length = new_line_size;
  document.lines[line_num+1].alloc  = new_line_size+1;

  /* fill the new line and clear the previous */
  strncpy(document.lines[line_num+1].str, 
          document.lines[line_num  ].str + col_num,
          new_line_size+1);
  document.lines[line_num].str[col_num] = '\0';
  document.lines[line_num].length = col_num;
}


void docRmvNewLine(int line_num) {
  char *tmp;
  int i;
  int total_length =  document.lines[line_num-1].length 
                     +document.lines[line_num].length;
  
  tmp = realloc(document.lines[line_num-1].str, total_length+1);
  if(tmp==NULL) {
    fprintf(stderr, "docActionBackspace failed to reallocate a line\n");
    finish(0);
  }
  document.lines[line_num-1].str   = tmp;
  document.lines[line_num-1].alloc = total_length+1;

  strncpy(document.lines[line_num-1].str + document.lines[line_num-1].length,
          document.lines[line_num].str,
          document.lines[line_num].length+1);

  document.lines[line_num-1].length = total_length;

  free(document.lines[line_num].str);
  for(i=line_num; i+1<document.nlines_used; ++i) {
    document.lines[i] = document.lines[i+1];
  }
  document.nlines_used -= 1;
}


void docAddChar(char c, int line_num, int col_num) {
  int i;
  if(document.lines[line_num].length+1 >= document.lines[line_num].alloc) {
    char *tmp = realloc(document.lines[line_num].str,
                        document.lines[line_num].alloc*2);
    if(tmp==NULL) {
      fprintf(stderr, "addCharToDocument failed to realloc a line\n");
      finish(0);
    }
    document.lines[line_num].str = tmp;
    document.lines[line_num].alloc *= 2;
  }

  document.lines[line_num].length += 1;
  for(i=document.lines[line_num].length; i>col_num; --i) {
    document.lines[line_num].str[i] = document.lines[line_num].str[i-1];
  }
  document.lines[line_num].str[col_num] = c;
}


void docRmvChar(int line_num, int col_num) {
  int i;
  for(i=col_num; i<document.lines[line_num].length; i++) {
    document.lines[line_num].str[i] = document.lines[line_num].str[i+1];
  }
  document.lines[line_num].length -= 1;
}






/********************* 
 * Edit View Control * 
 *********************/

int  moveViewDown(int dy);
void moveCursorDown(int dy);
void moveCursorRight(int dx);
void viewKeyActionEnter();
void viewKeyActionBackspace();
void viewKeyActionChar(char c);

struct {
  int screen_line;
  int screen_line_offset;
  int cursor_line;
  int cursor_col;
  WINDOW *main_win;
} view;


static int calculateScreenPos(int line_num, int col_num, int *y, int*x) {
  int screen_height, screen_width, screen_line, doc_line, doc_offset;

  getmaxyx(view.main_win, screen_height, screen_width);
  
  if(line_num > document.nlines_used) {
    return 1;
  }

  if(view.screen_line > line_num) {
    *y = -1;
    return 0;
  }

  if(view.screen_line == line_num && col_num < view.screen_line_offset*screen_width) {
    *y = -1;
    return 0;
  }

  screen_line = 0;
  doc_line = view.screen_line;
  doc_offset = view.screen_line_offset;
  while(doc_line < line_num || (doc_offset+1)*screen_width <= col_num) {
    screen_line += 1;
    doc_offset += 1;
    if(doc_offset*screen_width > document.lines[doc_line].length) {
      doc_line += 1;
      doc_offset = 0;
    }
  }

  *y = screen_line;
  *x = col_num % screen_width;
  return 0;
}


enum mode_e modeEdit() {
  int c;
  
  redrawwin(view.main_win);
  renderView();

  c = 0;
  while(c!=27) {
    c = getch();
    switch(c) {
      case KEY_DOWN:
        moveCursorDown(+1);
        renderView();
        break;
      case KEY_UP:
        moveCursorDown(-1);
        renderView();
        break;
      case KEY_LEFT:
        moveCursorRight(-1);
        renderView();
        break;
      case KEY_RIGHT:
        moveCursorRight(+1);
        renderView();
        break;
      case '\n':
        viewKeyActionEnter();
        renderView();
        break;
      case KEY_BACKSPACE:
        viewKeyActionBackspace();
        break;
      default:  
        if(c==KEY_F(2)) takeAction(&undo_list, &redo_list);
        if(c==KEY_F(3)) takeAction(&redo_list, &undo_list);
        if(c==KEY_F(10)) return MODE_MENU;

        if(c>=' ' && c<='~') {
          viewKeyActionChar(c);
        }
        break;
    }
  }
  return MODE_QUIT;
}

 
void initView() {
  view.screen_line = 0;
  view.screen_line_offset = 0;
  view.cursor_line = 0;
  view.cursor_col  = 0;

  view.main_win = NULL;
  refreshView();
}


int moveViewDown(int dy) {
  int screen_height, screen_width;
  
  getmaxyx(view.main_win, screen_height, screen_width);
  while(dy > 0) {
    dy -= 1;
    view.screen_line_offset += 1;
    if(view.screen_line_offset*screen_width > document.lines[view.screen_line].length) {
      if(view.screen_line+1 > document.nlines_used) {
        return 1;
      }
      view.screen_line += 1;
      view.screen_line_offset = 0;
    }
  }
  return 0;
}


void refreshView() {
  if(view.main_win) {
    delwin(view.main_win);
  }
  
  view.main_win = newwin(0, 0, 1, 0);
  renderView();
}


void renderView() {
  int screen_height, screen_width;
  int screen_line_num;
  int screen_line, screen_line_offset;
  int y, x;
  char *line_buff;
  
  getmaxyx(view.main_win, screen_height, screen_width);
  
  /* Ensure that the col pointed to exists */
  if(view.cursor_col > document.lines[view.cursor_line].length) {
    view.cursor_col = document.lines[view.cursor_line].length;
  }

  /* Ensure that the cursor is not above the screen */
  if(view.cursor_line < view.screen_line) {
    view.screen_line = view.cursor_line;
    view.screen_line_offset = view.cursor_col / screen_width;
  }

  if(view.cursor_line == view.screen_line) {
    if(view.cursor_col < view.screen_line_offset*screen_width) {
      view.screen_line_offset = view.cursor_col / screen_width;
    }
  }

  /* Ensure that the cursor is not below the screen */
  calculateScreenPos(view.cursor_line, view.cursor_col, &y, &x);
  while(y >= screen_height) {
    moveViewDown(1);
    calculateScreenPos(view.cursor_line, view.cursor_col, &y, &x);
  }

  /* Write document lines */
  line_buff = (char*) malloc(sizeof(char) * (screen_width + 1));

  screen_line = view.screen_line;
  screen_line_offset = view.screen_line_offset;
  screen_line_num = 0;
  while(screen_line_num < screen_height) {
    int line_width = document.lines[screen_line].length;
    if(line_width < screen_line_offset * screen_width) {
      screen_line += 1;
      screen_line_offset = 0;
      
      if(screen_line >= document.nlines_used) {
        break;
      }
    }
    strncpy(line_buff,
            document.lines[screen_line].str + screen_line_offset*screen_width,
            screen_width);
    
    line_buff[screen_width] = '\0';
    mvwprintw(view.main_win, screen_line_num, 0, "%s", line_buff);
    wclrtoeol(view.main_win);

    screen_line_offset += 1;
    screen_line_num += 1;
  }

  free(line_buff);

  /* finish */
  wmove(view.main_win, y, x);
  wrefresh(view.main_win);
}


void moveCursorDown(int dy) {
  int screen_height, screen_width;
  getmaxyx(view.main_win, screen_height, screen_width);

  if(dy > 0) {
    while(dy != 0) {
      int line_length = document.lines[view.cursor_line].length;
      if(line_length/screen_width <= view.cursor_col/screen_width) {
        if(view.cursor_line + 1 < document.nlines_used) {
          view.cursor_line += 1;
          view.cursor_col %= screen_width;
          line_length = document.lines[view.cursor_line].length;
          if(view.cursor_col > line_length) {
            view.cursor_col = line_length;
          }
        }
      } else {
        view.cursor_col += screen_width;
        line_length = document.lines[view.cursor_line].length;
        if(view.cursor_col > line_length) {
          view.cursor_col = line_length;
        }
      }
      dy -= 1;
    }
  }
  if(dy < 0) {
    while(dy != 0) {
      if(view.cursor_col < screen_width) {
        if(view.cursor_line != 0) {
          view.cursor_line -= 1;
          int line_length = document.lines[view.cursor_line].length;
          view.cursor_col += (line_length/screen_width)*screen_width;
        }
      } else {
        view.cursor_col -= screen_width;
      }
      dy += 1;
    }
  }
}


void moveCursorRight(int dx) {
  if(dx > 0) {
    while(dx != 0) {
      if(view.cursor_col < document.lines[view.cursor_line].length) {
        view.cursor_col += 1;
      } else {
        if(view.cursor_line != document.nlines_used - 1) {
          view.cursor_col = 0;
          view.cursor_line += 1;
        }
      }
      dx -= 1;
    }
  }
  if(dx < 0) {
    while(dx != 0) {
      if(view.cursor_col > 0) {
        view.cursor_col -= 1;
      } else {
        if(view.cursor_line != 0) {
          view.cursor_line -= 1;
          view.cursor_col = document.lines[view.cursor_line].length;
        }
      }
      dx += 1;
    }
  }
}


void viewKeyActionEnter() {
  struct action_s undo_action;

  docAddNewLine(view.cursor_line, view.cursor_col);
  view.cursor_line += 1;
  view.cursor_col = 0;
  renderView();
 
  undo_action.type = RMV_LINE;
  undo_action.line_num = view.cursor_line;
  pushAction(&undo_list, undo_action);
  destroyActionList(&redo_list);
}


void viewKeyActionBackspace() {
  struct action_s undo_action;
  if(view.cursor_col == 0) {
    if(view.cursor_line != 0) {
      view.cursor_col = document.lines[view.cursor_line-1].length;
      docRmvNewLine(view.cursor_line);
      view.cursor_line -= 1;
      renderView();

      undo_action.type = ADD_LINE;
      undo_action.line_num = view.cursor_line;
      undo_action.col_num  = view.cursor_col;
      pushAction(&undo_list, undo_action);
      destroyActionList(&redo_list);
    }
    
  } else {
    undo_action.c = document.lines[view.cursor_line].str[view.cursor_col-1];

    view.cursor_col -= 1;
    docRmvChar(view.cursor_line, view.cursor_col);
    renderView();

    undo_action.type = ADD_CHAR;
    undo_action.line_num = view.cursor_line;
    undo_action.col_num  = view.cursor_col;
    pushAction(&undo_list, undo_action);
    destroyActionList(&redo_list);
  }
}


void viewKeyActionChar(char c) {
  struct action_s undo_action;

  docAddChar(c, view.cursor_line, view.cursor_col);
  view.cursor_col += 1;
  renderView();

  undo_action.type = RMV_CHAR;
  undo_action.line_num = view.cursor_line;
  undo_action.col_num  = view.cursor_col-1;
  pushAction(&undo_list, undo_action);
  destroyActionList(&redo_list);
}



/**************** 
 * Menu System * 
 ****************/

#define OPEN_MENU_WIDTH 16

struct menu_lvl2_s {
  char *name;
  void (*function)();
};

struct menu_lvl1_s {
  char *name;
  struct menu_lvl2_s *items;
  int nitems;
};

struct {
  int cur_sel_1;
  int cur_sel_2;
  
  struct menu_lvl1_s *items;
  int nitems;

  enum mode_e next_mode;

  WINDOW *menu_bar;
  WINDOW *open_menu;
} menu = { .cur_sel_1=-1, .nitems=0 };

void menuItemFileQuit();
void menuItemEditUndo();
void menuItemEditRedo();


#define ALLOC_LVL1_MENU(INDEX,NUM)                                        \
  menu.items[INDEX].items = malloc(sizeof(struct menu_lvl2_s)*NUM);       \
  if(menu.items[INDEX].items==NULL) goto on_alloc_error;                  \
  menu.items[INDEX].nitems = NUM;

/* This function populates the menu structure.
 */
void buildMenu() {
  int i, n = 2;
  menu.items = malloc(sizeof(struct menu_lvl1_s)*n);
  if(menu.items == NULL) goto on_alloc_error;
  menu.nitems = n;

  for(i=0; i<n; ++i) {
    menu.items[i].nitems = 0;
  }
  
  /* FILE */
  menu.items[0].name = "File";
  ALLOC_LVL1_MENU(0,1)

  menu.items[0].items[0].name = " Quit     (esc) ";
  menu.items[0].items[0].function = &menuItemFileQuit;

  /* EDIT */
  menu.items[1].name = "Edit";
  ALLOC_LVL1_MENU(1,2)

  menu.items[1].items[0].name = " Undo      (F2) ";
  menu.items[1].items[0].function = &menuItemEditUndo;
  menu.items[1].items[1].name = " Redo      (F3)";
  menu.items[1].items[1].function = &menuItemEditRedo;
  
  return;

on_alloc_error:
  fprintf(stderr, "buildMenu failed to alloc\n");
  finish(0);
}


enum mode_e modeMenu() {
  int c;
  curs_set(0); /* set cursor invisible */
  menu.cur_sel_1 = 0;
  menu.cur_sel_2 = 0;
  renderMenuBar();

  menu.open_menu = newwin(menu.items[menu.cur_sel_1].nitems,
                          OPEN_MENU_WIDTH,
                          1, 1);
  
  renderMenuOpen();
  menu.next_mode = MODE_MENU;
  while(menu.next_mode==MODE_MENU) {
    c = getch();
    switch(c) {
      case KEY_DOWN: menu.cur_sel_2 += 1; break;
      case KEY_UP:   menu.cur_sel_2 -= 1; break;
      case KEY_LEFT: menu.cur_sel_1 -= 1; break;
      case KEY_RIGHT:menu.cur_sel_1 += 1; break;

      case '\n':
        menu.items[menu.cur_sel_1].items[menu.cur_sel_2].function();
        menu.next_mode = MODE_EDIT;
        break;
      default:
        if(c==27 || c==KEY_F(10)) {
          menu.next_mode = MODE_EDIT;
        }
        break;
    }

    if(menu.cur_sel_1 >= menu.nitems) menu.cur_sel_1 = menu.nitems-1;
    if(menu.cur_sel_1 < 0)            menu.cur_sel_1 = 0;

    if(menu.cur_sel_2 >= menu.items[menu.cur_sel_1].nitems)
      menu.cur_sel_2 = menu.items[menu.cur_sel_1].nitems-1;

    if(menu.cur_sel_2 < 0)
      menu.cur_sel_2 = 0;

    renderMenuBar();
    renderMenuOpen();
  }
  
  menu.cur_sel_1 = -1;
  delwin(menu.open_menu);
  renderMenuBar();
  curs_set(1); /* set cursor visible */

  return MODE_EDIT;
}


void initMenu() {
  buildMenu();

  menu.menu_bar = NULL;

  refreshMenu();
}


void refreshMenu() {
  if(menu.menu_bar) {
    delwin(menu.menu_bar); 
  }
  menu.menu_bar = newwin(1, 0, 0, 0);

  renderMenuBar();
}


void destroyMenu() {
  int i;

  for(i=0; i<menu.nitems; ++i) {
    free(menu.items[i].items);
  }

  if(menu.nitems!=0) {
    free(menu.items);
    menu.nitems = 0;
  }
}


void renderMenuBar() {
  int i;
  
  wmove(menu.menu_bar, 0, 0);

  wattron(menu.menu_bar, COLOR_PAIR(COLOR_PAIR_MENU));
  wprintw(menu.menu_bar, " ");
  for(i=0; i<menu.nitems; ++i) {
    if(i==menu.cur_sel_1) {
      wattron(menu.menu_bar, COLOR_PAIR(COLOR_PAIR_MENU_SELECTED));
    } else {
      wattron(menu.menu_bar, COLOR_PAIR(COLOR_PAIR_MENU));
    }
    wprintw(menu.menu_bar, " %s ", menu.items[i].name);
  }
  wattron(menu.menu_bar, COLOR_PAIR(COLOR_PAIR_MENU));
  wprintw(menu.menu_bar, " ");
  wrefresh(menu.menu_bar);
}


void renderMenuOpen() {
  int i,j;
  redrawwin(view.main_win);
  wrefresh(view.main_win);
  wresize(menu.open_menu, menu.items[menu.cur_sel_1].nitems, OPEN_MENU_WIDTH);
  for(i=0; i<menu.items[menu.cur_sel_1].nitems; ++i) {
    if(i==menu.cur_sel_2) {
      wattron(menu.open_menu, COLOR_PAIR(COLOR_PAIR_MENU_OPEN_SEL));
    } else {
      wattron(menu.open_menu, COLOR_PAIR(COLOR_PAIR_MENU_OPEN));
    }
    for(j=0; j<OPEN_MENU_WIDTH; ++j) {
      mvwprintw(menu.open_menu, i, j, " ");
    }
    mvwprintw(menu.open_menu, i, 0, menu.items[menu.cur_sel_1].items[i].name);
  }
  wrefresh(menu.open_menu);
}


void menuItemFileQuit() {
  finish(0);
}


void menuItemEditUndo() {
  takeAction(&undo_list, &redo_list);
}


void menuItemEditRedo() {
  takeAction(&redo_list, &undo_list);
}



/**************** 
 * Undo System * 
 ****************/

void initActionList(struct action_list_s *list) {
  list->front = NULL;
  list->back  = NULL;
  list->length = 0;
}


void destroyActionList(struct action_list_s *list) {
  struct action_s *action = list->front;
  struct action_s *tmp;

  while(action != NULL) {
    tmp = action->next;
    free(action);
    action = tmp;
  }

  initActionList(list);
}


void initUndoSystem() {
  initActionList(&undo_list); 
  initActionList(&redo_list); 
}


void destroyUndoSystem() {
  destroyActionList(&undo_list);
  destroyActionList(&redo_list);
}


void pushAction(struct action_list_s *list, struct action_s action) {
  struct action_s *tmp = malloc(sizeof(struct action_s));
  
  if(tmp==NULL) {
    fprintf(stderr, "pushAction failed to alloc\n");
    finish(0);
  }

  *tmp = action;
  tmp->next = list->front;

  if(list->length == 0) {
    list->back = tmp;
  } else {
    list->front->prev = tmp;
  }

  list->front = tmp;
  list->length += 1;

  if(list->length > MAX_UNDOABLE_ACTIONS) {
    tmp = list->back;
    tmp->prev->next = NULL;
    list->back = tmp->prev;
    free(tmp);
    list->length -= 1;
  }
}


struct action_s popAction(struct action_list_s *list) {
  struct action_s *next_node;
  struct action_s return_val;

  if(list->length==0) {
    return_val.type = NON_ACTN;
  } else {
    next_node = list->front->next;
    if(next_node) {
      next_node->prev = NULL;
    }
    return_val = *(list->front);
    free(list->front);
    list->front = next_node;

    list->length -= 1;
  }

  return return_val;
}


/* Takes an action from action_list and does it, putting the reverse action in the
 * undo_action_list.
 */
void takeAction(struct action_list_s *action_list,
                struct action_list_s *undo_action_list) {
  struct action_s action = popAction(action_list);
  switch(action.type) {
    case NON_ACTN:
      break;
    case ADD_LINE:
      docAddNewLine(action.line_num, action.col_num);
      view.cursor_line = action.line_num + 1;
      view.cursor_col  = 0;

      action.type = RMV_LINE;
      action.line_num = action.line_num+1;
      pushAction(undo_action_list, action);

      renderView();
      break;
    case RMV_LINE:
      view.cursor_line = action.line_num - 1;
      view.cursor_col = document.lines[action.line_num-1].length;
      docRmvNewLine(action.line_num);
      
      action.type = ADD_LINE;
      action.line_num = view.cursor_line;
      action.col_num  = view.cursor_col;
      pushAction(undo_action_list, action);

      renderView();
      break;
    case ADD_CHAR:
      docAddChar(action.c, action.line_num, action.col_num);
      view.cursor_line = action.line_num;
      view.cursor_col = action.col_num + 1;

      action.type = RMV_CHAR;
      action.line_num = action.line_num;
      action.col_num  = action.col_num;
      pushAction(undo_action_list, action);

      renderView();
      break;
    case RMV_CHAR:
      action.c = document.lines[action.line_num].str[action.col_num];

      docRmvChar(action.line_num, action.col_num);
      view.cursor_line = action.line_num;
      view.cursor_col  = action.col_num;

      action.type = ADD_CHAR;
      action.line_num = action.line_num;
      action.col_num  = action.col_num;
      pushAction(undo_action_list, action);

      renderView();
      break;
  }
}


