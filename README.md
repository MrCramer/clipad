# README #

A personal project to help me learn C. It's a text editor whose interface is
like notepads but implemented in the terminal with ncurses.

### What is this repository for? ###

* Text editor in the command line
* Using a curses "GUI"

### How do I get set up? ###

This is designed to be run in Unix, make sure you have the curses dev libraries
installed, modify the makefile to point to them with the flag variables and run
make.